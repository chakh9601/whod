const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const designerRouter = require('./router/designer_router');
const employerRouter = require('./router/employer_router');
const imageRouter =  require('./router/image_router');
const userRouter = require('./router/user_router');
const exec = require('child_process').exec;
const app = express();
const morgan = require('morgan');

app.use(bodyParser.urlencoded({ extended: false }));

app.use(morgan('<:method> :date User:remote-addr :url   :status'));

/*코드 자동화 */
app.post('/push',(req,res) => {
  if(req.method == 'POST')
    exec('cd ~/Project/whod');
    exec('sh atpull.sh');
    console.log('Pull success');
    res.send('pull success');
});

app.use(designerRouter);
app.use(employerRouter);
app.use(imageRouter);
app.use(userRouter);
//middleware about error
app.use(function(req, res, next) {
  res.sendStatus(404);
});

app.use(function(err, req, res, next) {
   res.status(500).send({msg: err.message});
});

app.listen(3000,function() {
  console.log('whod main server is running port : 3000');
});
