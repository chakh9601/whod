const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
var FCM = require('fcm-push');
const pool = require('./model/dbConnection');
const apiKey = 'AAAATwdKOqg:APA91bHFZXoKusO8a5niP_J_Phy5HdKlQ2ER4GOEr_q_ByVT3MOjjWKSyiUvc01SC7FEgaTvZGMpZtf_bSBJPvCy9hAhsFkmRnZjYtN2hzM3JcRzYrI0N2AcSpAGHlFQMYmAEMnJoMY4VN3YvX9sVcLD3Fg3ttvR9A';

const app = express();

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));


// 데이터베이스에서 토큰 가져와서 FCM에 발신 요청
// 알림 발송
app.post('/sendNoti', (req, res) => {

    var msg = req.body.msg;

    pool.getConnection((err, conn) => {
        var sql = 'select fcmid from user where user_id =1;';
        conn.query(sql, (err, results) => {
            if (err) {
                console.log('error sql : ', err);
                conn.release();
                return;
            }
            var tokens = results[0].fcmid;

            console.log('tokens : ', tokens, ' msg : ', msg);
            const message = {
                // 메세지를 받을 수 없는 상태
                // 전원 꺼진 상태, 네트워크 오프라인
                //delay_while_idle: true,  // 기기 비활성화 상태시 메세지 전송 늦추기
                time_to_live: 86400,   // 메세지 수명
                to: tokens,
                notification: {
                    title: 'FCM 알림 테스트',
                    text: msg
                },
                data: {
                    'value': '된다 된다 된다 될꺼야.'
                },
                //collapse_key: "collapse_key" // 전송되지 않은 기존 메세지를 최신 메세지로 대체 가능
            };

            const fcm = new FCM(apiKey);

            fcm.send(message, (err, results) => {

                if (err) {
                    console.error('Error : ', err);
                    res.status(500).send({ msg: err.message });
                    return;
                }
                res.send(results);
            });
        });
    });
});

app.listen(3000, function () {
    console.log('fcm server is running port : 3000');
});
