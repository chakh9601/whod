const app = require('express')();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const pool = require('./model/dbConnection');
const bodyParser = require('body-parser');
//const fcmObj = require('./model/fcm.js');
const async = require('async');
var dateFormat = require('dateformat');
require('date-utils'); //날짜 util


app.use(bodyParser.json());
server.listen(3001, function() {
    console.log('Chat Server listening port : 3001...');
});

// app.get('/', function(req, res) {
//     res.sendFile(__dirname + '/index.html');
// });

//on msg -> 클라이언트가 emit이벤트를 발생시키면 작동하는 이벤트
//emit msg-> 해당 이벤트를 클라이언트에 발생 시키는 이벤트
//즉,on 이벤트는 클라이언트->서버 요청
//emit이벤트는 서버->클라이언트 요청
class roomInfo {
    constructor(employer, designer, employerNick, designerNick) {
        this._employer = employer;
        this._designer = designer;
        this._employerNick = employerNick;
        this._designerNick = designerNick;
        this._employerStatus = -1;
        this._designerStatus = -1;
    }
};

var roomList = [];

//소켓접속
io.on('connection', function(socket) {
    console.log('connection');
    var socketroom;
    var nickname;
    var userpart;
    var id;
    //소켓에 누가 들어와있는지 확인하는 작업 ->
    //클라이언트에서 채팅방에 들어왔을때 발생되는 이벤트, 기존 메세지를 로딩해줌
    // data={
    //     chatRoomData:{
    //     history_id:매칭 히스토리 pk값
    //     designer_id:디자이너아이디
    //     employer_id:소상공인아이디
    //     employer_nickname:"해당 소상공인의 닉네임"
    //     designer_nickname:"해당 디자이너의 닉네임"
    //     },
    //     reqConnUserData:{
    //        userpart:0 or 1 //0 소상공인 1 디자이너
    //        id: part를 나누고 id값은 공통으로
    //      }
    //
    // }
    // {
    //     chatRoomData:{
    //         "history_id":1,
    //         "designer_id":1,
    //         "employer_id":2,
    //         "employer_nickname":"소상",
    //         "designer_nickname":"디자"
    //         }
    //     reqConnUserData:{
    //         "userpart":0,
    //         "id":1
    //     }
    //   }
    socket.on('joinRoom', function(data) {
        console.log(data);


        //채팅방 초기화
        //조건 구문 roominfo list 서칭,
        //-> 해당 룸 넘버가 존재 하면 바로 접속한 사람 여부 체크
        //-> 룸넘버가 존재하지 않으면 roomInfo객체 만들고 접속여부 체크
   //      {
   //    "history_id":1,
   //    "designer_id":1,
   //    "employer_id":2,
   //    "employer_nickname":"소상",
   //    "designer_nickname":"디자",
   //    "userpart":0,
   //    "id":1
   // }

        var select_sql;
        var update_sql;
        socketroom = data.consult_id;
        if (roomList[ socketroom] === undefined) {
            var obj = new roomInfo(
                data.employer_id,
                data.designer_id,
                data.employer_nickname,
                data.designer_nickname);
            roomList[socketroom] = obj;
        }
        console.log(roomList[socketroom]);
        userpart = data.userpart;

        if (userpart == 0){
             nickname = data.employer_nickname;
             roomList[socketroom]._employerStatus=1;
             console.log('userpart -> 0',roomList[socketroom]._employerStatus);
         }
        else if (userpart == 1){
             nickname = data.designer_nickname;
             roomList[socketroom]._designerStatus=1;
              console.log('userpart -> 1 ',roomList[socketroom]._designerStatus);
         }
        id = data.id;


        // switch (userpart) {
        //     case 0:
        //         select_sql='select msg_from,contents,msg_time from chatmessage where consult_id= ? and listen_e=0;';
        //         break;
        //     case 1:
        //         select_sql='select msg_from,contents,msg_time from chatmessage where consult_id= ? and listen_d=0;';
        //     break;
        //
        //     default:
        // }
        select_sql = 'select msg_from,contents,msg_time from chatmessage where consult_id= ?';
        socket.join(socketroom);

        // if (userpart != 0 && userpart != 1) {
        //     socket.emit('error', {
        //         msg: "userpart input number error"
        //     });
        //     return;
        // }

        pool.getConnection((err, conn) => {
            async.waterfall(
                [
                    function selectMsg(innercallback) {

                        conn.query(select_sql, socketroom, (err, result) => {
                            if (err) {
                                return innercallback(err);
                            }
                            var msg_obj = {};
                            msg_obj.msg = "success";
                            msg_obj.number = result.length; //client length ==0 처리 해주야함
                            msg_obj.list = result;
                            //console.log(msg_obj);
                            innercallback(null, msg_obj);
                        });
                    },
                    function updateMsg(data, innercallback) {
                        if (userpart == 0) update_sql = 'update chatmessage set listen_e=1 where consult_id= ? and (listen_e=0 or listen_e is null);';
                        else if (userpart == 1) update_sql = 'update chatmessage set listen_d=1 where consult_id= ? and (listen_d=0 or listen_d is null)'
                        conn.query(update_sql, socketroom, (err, result) => {
                            if (err) {
                                return innercallback(err);
                            }
                            innercallback(null, data);
                        });
                    }
                ],
                function(err, data) {
                    if (err) {
                        conn.release();
                        socket.emit('error', {
                            msg: 'joinRoom error'
                        });
                        return;
                    }
                    conn.release();
                    socket.emit('msglist', data);
                }
            );
        });
    });

    //클라이언트에서 새로운 메세지가 입력되었을때 발생되는 이벤트
    // data={
    //    msg_from:0 or 1  -> 0:소상공인 1,디자이너
    //    message : "message" ->클라이언트에서 발생된 메세지

    // }
    socket.on('chatInput', function(data) { //data는 json타입으로 들어와야함.
        if (socketroom === undefined) {
            socket.emit('error', {
                msg: "joinRoom Event not occured"
            });
            return;
        }
	console.log(data);
        var dt = new Date();
        var msg_from = data.msg_from;
        var msg = data.message;
        var sql;
        var chat = {
            msg: msg,
            msg_from: msg_from,
            time: dt
        };
        //둘다 접속해 있지 않는 경우 ->
        if (roomList[socketroom]._employerStatus == -1 && roomList[socketroom]._designerStatus == -1) {
            socket.emit('error', {
                msg: "invalid connection "
            });
            return;
        } else {
            //둘다 접속해 있는 경우
            if (roomList[socketroom]._employerStatus != -1 && roomList[socketroom]._designerStatus != -1) {
                sql = 'insert into chatmessage values(null,?,?,?,?,1,1);';
            }
            //소상공인만 접속해 있는 경우
            else if (roomList[socketroom]._employerStatus != -1) {
                sql = 'insert into chatmessage values(null,?,?,?,?,0,1);';
                // 디자이너에게 푸시
                // fcmObj.sendMsgPush(1,roomList[socketroom]._designer,(err,result) => {
                //     if(err){
                //         console.log('fcm error');
                //         return;
                //     }
                //     console.log('push to designer number : ',roomList[socketroom]._designer);
                // });

            }
            //디자이너만 접속해 있는 경우
            else if (roomList[socketroom]._designerStatus != -1) {
                sql = 'insert into chatmessage values(null,?,?,?,?,1,0);';
                //소상공인에게 푸시
                // fcmObj.sendMsgPush(0,roomList[socketroom]._employer,(err,result) => {
                //     if(err){
                //         console.log('fcm error');
                //         return;
                //     }
                //     console.log('push to employer number : ',roomList[socketroom]._designer);
                // });
            }
        }
        pool.getConnection(function(err, conn) {

            conn.query(sql, [socketroom, msg_from, msg, dt], (err, result) => {
                if (err) {
                    conn.release();
                    console.log(err);
                    socket.emit('error', {
                        msg: 'query error'
                    });
                }
                conn.release();
                io.to(socketroom).emit('chatMessage', chat);
            });
        });
        console.log(nickname + '(' + socketroom + ') >> ' + msg);
    });
    /*
    data ={
        userpart: 0 or 1
    }
    */
    socket.on("disconnect", function(data) {
        // if (roomList[socketroom]._employerStatus == -1 && roomList[socketroom]._designerStatus == -1) {
        //     socket.emit('error', {
        //         msg: "invalid connection "
        //     });
        //     return;
        // }
        switch (userpart) {
            case 0:
                roomList[socketroom]._employerStatus = -1;
                break;
            case 1:
                roomList[socketroom]._designerStatus = -1;
                break;
            default:

        }
        console.log('user : ' + nickname + " disconnect");
    });
});
