﻿# DEV_JOURNAL #

### 작성예 ###
* <날짜> dev issue #번호 : 커밋명
설명


## <2016-11-21> dev issue #1 : first commit
  - 코드 자동 pull 적용 (bitbucket webhooks POST 요청 이용)
  - SSH등록
  - develop, hotfix, master branchs
  - README 수정
  - Designer.getDesignerInfo 메소드 결과값 form 수정

## <2016-11-22> dev issue #2 : ADD Designer API
  - DesignerList 응답 요청 작성, 추후에 재작업 필요
  - ReviewMore 응답 요청 작성
  - 주석작
  - designer관련 더미데이터 DB에 추가
  - DB 테이블 수정

## <2016-11-23> dev issue #3 : ADD S3 API
  - ImageUpload를 다루는 모듈 작업
  - router 구체화

## <2016-11-24> dev issue #4 : ADD Folder API
  - 회원가입 시 디자이너 id, portpolio / 소상공인 id, storeimg 폴더 생성
  - 회원가입 API는 추후에 작성할 예정이며, 해당 API는 회원가입 절차를 거친 이후에 대한 처리

## <2016-11-24> dev issue #5 : Update S3 API, Update designer router
  - S3 작업 세부 수정 마무리 DONE
  - desinger 모델 일부 test 결과 내주는 api, 완성형으로 변경
  - 포미더블 버그 수정

## <2016-11-27~28> dev issue #6 :
  - 디자이너 레벨 get api 작성
  - 응답 데이터 포맷 변경
  - designer 리뷰 작성 api
  - 디자이너 put rcmd 작성
  - 이미지 업로드시 확장자 삭제

## <2016-11-28> dev issue #7 :  ADD employer API
 - 소상공인 프로필 보기 => profile, review(limit 3), reviewavg
 - 추천 조건 입력, 추천 조건 보기

## <2016-11.28> dev issue #8 : Update employer API
 - 찜 목록 보기, 찜 추가, 찜 해제
 - 찜 추가, 찜 해제 응답(res) 수정

## <2016-11-29> dev issue #10 : add employer model, router
 - 소상공인 리뷰 GET (Reviewmore), 리뷰 작성 POST
 - 다시 쿼리할 때 format 형식 수정
 - 소상공인 프로필 => 스타일 태그, 추천 조건 GET 추가

## <2016-11-30> dev issue #11 : Update employer postEmployerRecommend
 - 매칭 시스템 완성 (데이터 방대하게 만들어야 함)

## <2016-12-01> dev issue #12 : add employer matchingHistory
 - 매칭 결과 매칭 히스토리에 삽입
 - 소상공인별 매칭 히스토리 보기

## <2016-12-03> dev issue #13 : Update employer get matchingHistory, Add Log module
 - morgan 모듈 이용한 log작성
 - 매칭 히스토리 get 메소드 수정
