"use strict";

var pool = require('./dbConnection');
const AWS = require('aws-sdk');
const config = require('../config/s3config.js');
const async = require('async');
var dateFormat = require('dateformat');
// var jwt = require('jwt-simple');
//const secretkey = 'WhoDSecretlf1612an';

require('date-utils'); //날짜 util

AWS.config.region = config.region;
AWS.config.accessKeyId = config.accessKeyId;
AWS.config.secretAccessKey = config.secretAccessKey;

var s3 = new AWS.S3();

class User{
}
//email 중복 체크 DONE
User.emailcheck = (data,callback) => {
    pool.getConnection(function(err,conn){
        if(err){
            conn.release();
            console.log(err);
            return callback(err);
        }
        conn.query('select email from user where email = ?',data,function(err,result){
            if(err){
                conn.release();
                console.log('query err : ',err);
                return callback(err);
            }
            conn.release();
            if(result[0]===undefined) return callback(null,{msg:'nothing'});
            else return callback(null,{msg:'duplicate'});
        });
    });
}

/*User.univcheck = (data,callback) => {
    pool.getConnection(function(err,conn){
        if(err){
            console.log(err);
            return callback(err);
        }
        conn.query('select univ_name from univlist where = ?;',data,(err,result) => {
            if(err){
                conn.release();
                console.log('query err',err);
                return callback(err);
            }
            conn.release();
            return callback(null,result[0]);
        });
    });
}*/

User.postNewDesigner = (obj,callback) => {
    pool.getConnection(function(err, conn){
        async.waterfall(
            [
                function insertDesignerDefaultData(innercallback){
                    console.log(typeof(obj.nickname),typeof(obj.auth+0),typeof(obj.univ+0),typeof(obj.admissiondate));
                    var sql = 'insert into designer values(null,?,0,1,0,0,?,\'자기소개를 입력하세요.\',null,'+
                    '0,1,null,0,?,?)';
                    conn.query(sql,[obj.nickname,obj.auth+0,obj.univ+0,obj.admissiondate],function(err,result){
                        if(err){
                            conn.release();
                            console.log('query err : ',err);
                            return callback(err);
                        }
                        innercallback(null);
                    });
                },
                function getId(innercallback){

                    var sql = 'SELECT LAST_INSERT_ID() as lastID;';
                    conn.query(sql,function(err,result){
                        if(err){
                            conn.release();
                            console.log('query err : ',err);
                            return callback(err);
                        }
                        innercallback(null,result[0].lastID);
                    });

                },
                function insertUserData(pk,innercallback){

                    var dt = new Date();
                    var d = dt.toFormat('YYYY-MM-DD');
                    var sql = 'insert into user values(null,?,?,?,?,null,?,?,null);';
                    conn.query(sql,[obj.email,obj.password,obj.name,pk,obj.phonenumber,d],function(err,result){
                        if(err){
                            conn.release();
                            console.log('query err : ',err);
                            return callback(err);
                        }
                        conn.release();
                        innercallback(null,pk);
                    });     //conn.query
                },
                function TEMP_createS3Obj(id,innercallback){
                    var foldername = 'designer/' + id + '/portpolio/';

                    var params = {
                        Bucket : 'whod',
                        Key : foldername,
                        ACL : 'public-read',
                        Body : 'body does not matter'
                    };

                    s3.upload(params, function(err, result){
                        if(err){
                            console.log('Error Creating The Folder: ', err);
                            return callback(err, null);
                        }
                        console.log('create designer s3 object :',foldername);
                        innercallback(null, foldername);
                    });

                }
            ],
            function (err,result){
                if(err){
                    console.log('result err : ',err);
                    return callback(err);
                }
                var resData = {
                    msg: 'designer signup complete'
                };
                return callback(null, resData);
            }
        );
    });
}


User.postNewEmployer = (obj,callback) => {
    console.log(obj);
    pool.getConnection(function(err,conn){
        async.waterfall(
            [

                function insertEmployerDefaultData(innercallback){
                    var sql = 'insert into employer values(null,?,0,0,?,null,\'자기소개를 입력하세요\');';
                    conn.query(sql,[obj.nickname,obj.auth],function(err,result){
                        if(err){
                            conn.release();
                            console.log('query error : ',err);
                            return callback(err);
                        }
                        innercallback(null,result);
                    });
                },
                function getId(result,innercallback){
                    var sql = 'select LAST_INSERT_ID() as lastID;';
                    conn.query(sql,function(err,result){
                        if(err){
                            conn.release();
                            console.log('query error : ',err);
                            return callback(err);
                        }
                        innercallback(null,result[0].lastID);
                    });
                },
                function insertUserData(pk,innercallback){
                    var dt = new Date();
                    var d = dt.toFormat('YYYY-MM-DD');
                    var sql = 'insert into user values(null,?,?,?,null,?,?,?,null);';
                    conn.query(sql,[obj.email,obj.password,obj.name,pk,obj.phonenumber,d],function(err,result){
                        if(err){
                            conn.release();
                            console.log('query err : ',err);
                            return callback(err);
                        }
                        conn.release();
                        innercallback(null,pk);
                    });     //conn.query

                },
                function TEMP_createS3Obj(id,innercallback){
                    var foldername = 'employer/' + id + '/storeimg/';
                    var params = {
                        Bucket : 'whod',
                        Key : foldername,
                        ACL : 'public-read',
                        Body : 'body does not matter'
                    };

                    s3.upload(params, function(err, result){
                        if(err){
                            console.log('Error Creating The Folder: ', err);
                            return callback(err, null);
                        }
                        console.log('create employer s3 object :',foldername);
                        innercallback(null, foldername);
                    });
                }
            ],
            function (err,result){
                if(err){
                    console.log('result err : ',err);
                    return callback(err);
                }
                var resData = {
                    msg: 'employer signup complete'
                };
                return callback(null, resData);
            }
        );
    });
}

User.SignIn = (email,password,callback) =>{
    pool.getConnection(function(err,conn){
        var sql = 'select email,password,designer_id,employer_id from user where email =?';
        conn.query(sql,email, (err,result) => {
            if(err){
                conn.release();
                console.log('query err : ',err);
                return callback(err);
            }
            if(result[0] === undefined ){
                conn.release();
                return callback(null,'email check failed');
            }
            else {
                if(password == result[0].password){
                    conn.release();
                    if(result[0].designer_id!==null)return callback(null, {part:0,designer_id:result[0].designer_id});
                    else if(result[0].employer_id!==null)return callback(null,{part:1,employer_id:result[0].employer_id});
                }
                else{
                    conn.release();
                    return callback(null,'password check failed');
                }
            }
        });
    });
}

/*
set = 1 : designer create folder
set = 2 : employer create folder
*/
User.postJoin = (set, id, resultcallback) => {

    var foldername;

    async.waterfall([
        function settings(callback){
            switch(set){
                case 1:
                    console.log('set code 1');
                    foldername = 'designer/' + id + '/portpolio/';
                    break;
                case 2:
                    console.log('set code 2');
                    foldername = 'employer/' + id + '/storeimg/';
                    break;
            }
            callback(null, 1);
        },
        function(value, callback){

            var params = {
                Bucket : 'whod',
                Key : foldername,
                ACL : 'public-read',
                Body : 'body does not matter'
            };

            s3.upload(params, function(err, result){
                if(err){
                    console.log('Error Creating The Folder: ', err);
                    return callback(err, null);
                }
                callback(null, foldername);
            });
        }
    ],
    function (err, result){
        if(err){
          return resultcallback(err);
        }
        console.log('Successfully Created A Folder On S3 : ', foldername);
        return resultcallback(null,'Success');
    });
}

module.exports = User;
