var pool = require('./dbConnection');
var async = require('async');
var dateFormat = require('dateformat');

require('date-utils'); //날짜 util

class Employer { }
Employer.getEmployerInfo = (e_id,callback) => {
    pool.getConnection( (err, conn) => {
        if(err){
            console.log('error connecting : ', err);
            conn.release();
            return callback(err);
        }
        var sql = 'select nickname,introduce, storeimg_number from employer where employer_id =?;'
        conn.query(sql,e_id,(err,result) =>{
            if (err) {
              console.log('error sql : ', err);
              conn.release();
              return callback(err);
            }
            conn.release();
            var obj ={profile:{}};
            obj.profile=result[0];
            return callback(null,obj);
        });
    });
}
//소상공인 프로필 보기
Employer.getEmployerInfoDetail = (e_id, callback) => {
  pool.getConnection(function (err, conn) {
    if (err) {
      console.log('error connecting : ', err);
      conn.release();
      return callback(err);
    }

    async.series([
      function profile(innercallback) { //기본 프로필
        var sql = 'select e.nickname, e.storeimg_number, e.point, e.auth, e.introduce, recommend.corpctg_id, recommend.w1ctg_id, recommend.w2ctg_id,' +
          'recommend.mintime, recommend.maxtime, recommend.minwage, recommend.maxwage, recommend.sttag1_id, recommend.sttag2_id, recommend.sttag3_id ' +
          'from employer as e inner join rcmdemployer as recommend where e.employer_id = ?;';

        conn.query(sql, e_id, (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          else if(results[0] == undefined){
            return innercallback({message : 'employer id does not exist.'});
          }

          var recommend = {
            nickname: results[0].nickname,
            storeimg_number: results[0].storeimg_number,
            point: results[0].point,
            auth: results[0].auth,
            introduce: results[0].introduce,
            corpctg_id: results[0].corpctg_id,
            work: [results[0].w1ctg_id, results[0].w2ctg_id],
            mintime: results[0].mintime,
            maxtime: results[0].maxtime,
            minwage: results[0].minwage,
            maxwage: results[0].maxwage,
            style: [results[0].sttag1_id, results[0].sttag2_id, results[0].sttag3_id]
          };

          innercallback(null, recommend);
        });
    }, //function profile
      function review(innercallback) { //리뷰 3개
        var sql = 'select designer_id, contents, score, reviewdate from ereview where employer_id = ? order by(reviewdate) desc limit 3;';
        conn.query(sql, e_id, (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          innercallback(null, results);
        });
      },
    ],
      function (err, results) {
        if (err) {
          conn.release();
          return callback(err);
        }

        for (var i = 0; i < results[1].length; i++) {
          var str = dateFormat(results[1][i].reviewdate, 'yyyy-mm-dd');
          results[1][i].reviewdate = str;
        }
        var obj = {
          profile: {}, review: []
        };

        obj.profile = results[0];
        obj.review = results[1];

        conn.release();
        callback(null, obj);
      }
    ); //async.series
  }); //pool.Connection
} //Employer.getEmployerInfo


//소상공인 리뷰 보기
Employer.getReviewMore = (e_id, callback) => {
  pool.getConnection(function (err, conn) {
    if (err) {
      console.log('error connecting :', err);
      conn.release();
      return callback(err);
    }

    async.waterfall([
      function getID(innercallback){
        var sql = 'select employer_id as `eid` from employer where employer_id = ?;';
        conn.query(sql, e_id, (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          else if(results[0] == undefined){
            return innercallback({message : 'employer id does not exist.'});
          }
          innercallback(null, results[0].eid);
        });
      },
      function showAllReview(e_id, innercallback) {

        var sql = 'select designer_id, contents, score, reviewdate from ereview where employer_id = ? order by(reviewdate) desc;';
        conn.query(sql, e_id, (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }

          for (var i = 0; i < results.length; i++) {
            var str = dateFormat(results[i].reviewdate, 'yyyy-mm-dd');
            results[i].reviewdate = str;
          }

          var obj = {
            reviewlist: []
          };

          for (var i = 0; i < results.length; i++) {
            obj.reviewlist[i] = results[i];
          }

          innercallback(null, obj);
        });
      }
    ],
    function (err, results) {
        if (err) {
          console.log('error : ', err);
          conn.release();
          return callback(err);
        }
        conn.release();
        callback(null, results);
      });
  }); //pool.Connection
} //Employer.getReviewMore


//소상공인 리뷰 작성
Employer.postReview = (e_id, review, callback) => {

  pool.getConnection(function (err, conn) {
    if (err) {
      console.log('error connecting : ', err);
      conn.release();
      return callback(err);
    }

    async.waterfall([
      function getEmployerID(innercallback) {
        var sql = 'select employer_id as `eid` from employer where employer_id = ?;';
        conn.query(sql, e_id, (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          else if(results[0] == undefined){
            return innercallback({message : 'employer id does not exist.'});
          }
          innercallback(null, results[0].eid);
        });
      },
      function getDesignerID(e_id, innercallback) {
        var sql = 'select designer_id as `did` from designer where designer_id = ?;';
        conn.query(sql, review.designer_id, (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          else if(results[0] == undefined){
            return innercallback({message : 'designer id does not exist.'});
          }
          innercallback(null, e_id, results[0].did);
        });
      },
      function insertReview(e_id, d_id, innercallback) {
        var dt = new Date();
        var d = dt.toFormat('YYYY-MM-DD');

        var sql = 'insert into ereview values(null,?,?,?,?,?);';
        conn.query(sql, [e_id, d_id, review.contents, review.score, d], (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          innercallback(null, results);
        });
      }
    ],
      function (err, results) {
        if (err) {
          console.log('error : ', err);
          conn.release();
          return callback(err);
        }
        var resData = {
          msg: 'review insert success'
        };

        conn.release();
        callback(null, resData);
      }); //async.waterfall
  }); //pool.getConnection
} //Employer.postReview


//추천 조건 입력과 매칭
Employer.postEmployerRecommend = (e_id, rcmd, callback) => {

  pool.getConnection(function (err, conn) {
    if (err) {
        console.log('error connecting : ', err);
        conn.release();
        return callback(err);
    }
    async.waterfall([
      function getID(innercallback) {
        var sql = 'select employer_id as `eid` from employer where employer_id = ?;';
        conn.query(sql, e_id, (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          else if(results[0] == undefined){
            return innercallback({message : 'employer id does not exist.'});
          }
          innercallback(null, results[0].eid);
        });
      },
      function insertRecommend(e_id, innercallback) {
        var dt = new Date();
        var d = dt.toFormat('YYYY-MM-DD');

        var sql = 'insert into rcmdemployer values(null, ?, ?, null, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);';

        conn.query(sql, [e_id, rcmd.corp, rcmd.work[0], rcmd.work[1], rcmd.mintime, rcmd.maxtime, rcmd.minwage,
          rcmd.maxwage, rcmd.style[0], rcmd.style[1], rcmd.style[2], d], (err, results) => {
            if (err) {
              console.log('error sql : ', err);
              return innercallback(err);
            }
            innercallback(null, 'success');
          });
      },
      function getID(params, innercallback) {
        //recommend_id 반환
        var sql = 'SELECT LAST_INSERT_ID() as lastID;';

        conn.query(sql, (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          innercallback(null, results[0].lastID);
        });
      },
      function updateEmployerTable(params, innercallback) {
        var sql = 'update employer set recommend_id =? where employer_id = ?;';
        conn.query(sql, [params, e_id], (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }

          innercallback(null, 'success');
        });
      },
      //매칭 알고리즘
      function getMatching(params, innercallback) {
        //TODO
        //기존 매칭 기록 가져와서 중복 제거

        var sql = 'select rcmd.designer_id, d.nickname,rcmd.corp1ctg_id as corp1, rcmd.corp2ctg_id as corp2, rcmd.w1ctg_id as work1, rcmd.w2ctg_id as work2, '+
        'rcmd.mintime, rcmd.mintime,rcmd.maxtime,rcmd.minwage,rcmd.maxwage,' +
          'rcmd.sttag1_id as style1,rcmd.sttag2_id as style2,rcmd.sttag3_id as style3 ' +
          'from rcmddesigner as rcmd inner join designer as d on rcmd.designer_id=d.designer_id where corp1ctg_id = ? or corp2ctg_id= ?;';

        conn.query(sql, [rcmd.corp, rcmd.corp], (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          var obj = { list: [] };
          obj.list = results;
          //console.log('obj.list.length : ', obj.list.length);

          var firstPriorityGroup = new Array();
          var secondPriorityGroup = new Array();
          var otherGroup = new Array();
          for (i = 0; i < obj.list.length; i++) {
            var tagnum = 0;
            var matchingTag = Array();
            var worknum = 0;

            var matchingWork = Array();
            for (j = 0; j < rcmd.style.length; j++) {
              if (obj.list[i].style1 == rcmd.style[j]) {
                tagnum++;
                matchingTag.push(obj.list[i].style1);
              }
              if (obj.list[i].style2 == rcmd.style[j]) {
                tagnum++;
                matchingTag.push(obj.list[i].style2);
              }
              if (obj.list[i].style3 == rcmd.style[j]) {
                tagnum++;
                matchingTag.push(obj.list[i].style3);
              }
            }
            // console.log('tagnum', tagnum);
            // console.log('matchingTag', matchingTag);
            obj.list[i].matchingdata = {};
            obj.list[i].matchingdata.tagnum = tagnum;
            obj.list[i].matchingdata.matchingTag = [];
            obj.list[i].matchingdata.matchingWork = [];
            obj.list[i].matchingdata.matchingTag = matchingTag;

            if (obj.list[i].work1 == rcmd.work[0]) {
              matchingWork.push(obj.list[i].work1);
              worknum++;
            }
            if (obj.list[i].work1 == rcmd.work[1]) {
              matchingWork.push(obj.list[i].work1);
              worknum++;
            }
            if (obj.list[i].work2 == rcmd.work[0]) {
              matchingWork.push(obj.list[i].work2);
              worknum++;
            }
            if (obj.list[i].work2 == rcmd.work[1]) {
              matchingWork.push(obj.list[i].work2);
              worknum++;
            }
            obj.list[i].matchingdata.worknum = worknum;
            obj.list[i].matchingdata.matchingWork = matchingWork;

          }//main loop

          /*우선순위 그룹에 매칭 결과 삽입*/
          for (i = 0; i < obj.list.length; i++) {
            if (obj.list[i].matchingdata.tagnum == 3) {

              if (obj.list[i].matchingdata.worknum == 2) {

                firstPriorityGroup.unshift(obj.list[i]);
              }
              else if (obj.list[i].matchingdata.worknum == 1) {
                firstPriorityGroup.push(obj.list[i]);
              }
            }
            if (obj.list[i].matchingdata.tagnum == 2) {
              if (obj.list[i].matchingdata.worknum == 2) secondPriorityGroup.unshift(obj.list[i]);
              else if (obj.list[i].matchingdata.worknum == 1) secondPriorityGroup.push(obj.list[i]);
            }
            if (obj.list[i].matchingdata.tagnum == 1) {
              otherGroup.push(obj.list[i]);
            }

          }//second loop
          console.log(firstPriorityGroup);
          console.log(secondPriorityGroup);
          console.log(otherGroup);
            /*데이터 잘 들어옴*/

          var resultArray = {
                firstPrior: firstPriorityGroup.length,
                secondPrior: secondPriorityGroup.length,
                other: otherGroup.length,
                length:{},
                list:[]
          };
          resultArray.length=0;


          while (resultArray.length<5) {
            if (firstPriorityGroup.length === 0 && secondPriorityGroup.length === 0 && otherGroup.length === 0) {
              //console.log('5개 안채워진 경우');

              break;
            }
            if (firstPriorityGroup.length != 0) resultArray.list.push(firstPriorityGroup.pop());
            else if (secondPriorityGroup.length != 0) resultArray.list.push(secondPriorityGroup.pop());
            else if (otherGroup.length != 0) resultArray.list.push(otherGroup.pop());
            resultArray.length++;
          }//while

          //TODO
          //우선순위 그룹을 취합시키기
          //취합 조건 만들어야함
          //결과 콜백으로 보내기

          return innercallback(null, resultArray);
        });
      }
    ],
      function (err, results) {
        if (err) {
          console.log('error : ', err);
          conn.release();
          return callback(err);
        }
        conn.release();
        callback(null, results);
      }); //async.waterfall
  }); //pool.getConnection
} //Employer.postEmployerRecommend


//디자이너에게 작업 상담 요청
Employer.postDesignerMatching = (e_id, obj, callback) => {
  pool.getConnection(function (err, conn) {

    var dt = new Date();
    var d = dt.toFormat('YYYY-MM-DD');

    if (err) {
      console.log('connectiong err : ', err);
      conn.release();
      return callback(err);
    }

    //매칭 기록에 삽입
    async.waterfall(
        [
            function dataInsertion(innercallback){
                var sql = 'insert into matchinghistory values(null,?,?,?,?);';
                conn.query(sql,[obj.designer_id,e_id,d,obj.recommend_id], (err, results) => {
                  if(err){
                    console.log('error sql : ', err);
                    innercallback(err);
                  }
                  innercallback(null);
                });
            },
            function lastInsertId(innercallback){
                var sql = 'select LAST_INSERT_ID() as lastID;';
                conn.query(sql,(err,result) => {
                    if(err){
                        conn.release();
                        innercallback(err);
                    }

                    innercallback(null,result[0]);
                });
            },
            function resDataMaking(result,innercallback){
                var data ={
                    matchinghistory_id:{},
                    designer_id:{}
                };
                data.matchinghistory_id=result;
                data.designer_id=obj.designer_id;
                innercallback(null,data);
            }
        ],
        function(err,result){
            if(err){
                conn.release();
                return callback(err);
            }
            conn.release();
            return callback(null,result);
        }
    );
    //채팅방 만들기********
    //primarykey 획득(history_id)
  });//pool.getConnection
}//Employer.postDesignerMatching


//매칭 히스토리 보기
Employer.getMatchingHistory = (e_id, callback) => {
  pool.getConnection(function(err, conn) {
    if(err){
      console.log('connectiong err : ', err);
      conn.release();
      return callback(err);
    }

    async.waterfall([
      function getID(innercallback) {
        var sql = 'select employer_id as `eid` from employer where employer_id = ?;';
        conn.query(sql, e_id, (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          else if(results[0] == undefined){
            return innercallback({message : 'employer id does not exist.'});
          }
          innercallback(null, results[0].eid);
        });
      },
      function showMatchingHistory(e_id, innercallback) {
        var sql = 'select m.designer_id as designer_id, d.nickname,rcmd.corp1ctg_id as corp1,rcmd.corp2ctg_id as corp2,rcmd.w1ctg_id as work1,' +
          'rcmd.w2ctg_id as work2, rcmd.minwage as minwage, rcmd.maxwage as maxwage, ' +
          'rcmd.mintime as mintime, rcmd.maxtime as maxtime, rcmd.sttag1_id as style1, rcmd.sttag2_id as style2, ' +
          'rcmd.sttag3_id as style3 from matchinghistory as m inner join rcmddesigner as rcmd on m.rcmddesigner_id = rcmd.recommend_id ' +
          'inner join designer as d on m.designer_id=d.designer_id where m.employer_id = ?;';

        conn.query(sql, e_id, (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          var obj = {
            designer: []
          };

          for (var i = 0; i < results.length; i++) {
            obj.designer[i] = results[i];
          }
          obj.list_num = results.length;

          innercallback(null, obj);
        });
      }
    ],
      function (err, results) {
        if (err) {
          console.log('error : ', err);
          conn.release();
          return callback(err);
        }
        conn.release();
        callback(null, results);
      }); //async.waterfall
  }); //pool.getConnection
} //Employer.getMatchingHistory

// Employer.getConsultingHistory = (e_id,callback) => {
//     pool.getConnection(function (err,conn) {
//         var sql =' select history.consult_id, history.employer_id, history.designer_id,msg.contents,msg.msg_time from consultinghistory as history' +
//         ' inner join chatmessage as msg on history.consult_id=msg.consult_id where history.employer_id = ? order by msg.msg_time desc limit 1;';
//         conn.query(sql,e_id,(err,result)=>{
//             if(err){
//                 console.log(err);
//                 return callback(err);
//             }
//             data = {list:result[0]
//         });
//     });
// }


//추천 조건 보기
Employer.getEmployerRecommend = (e_id, callback) => {

  pool.getConnection(function (err, conn) {
    if (err) {
      console.log('error connecting : ', err);
      conn.release();
      return callback(err);
    }
    async.waterfall([
      function getID(innercallback) {
        var sql = 'select employer_id as `eid` from employer where employer_id = ?;';
        conn.query(sql, e_id, (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          else if(results[0] == undefined){
            return innercallback({message : 'employer id does not exist.'});
          }
          innercallback(null, results[0].eid);
        });
      },
      function showRecommendation(e_id, innercallback) {
        var sql = 'select rcmd.corpctg_id, rcmd.w1ctg_id, rcmd.w2ctg_id, rcmd.mintime, rcmd.maxtime, rcmd.minwage,' +
          'rcmd.maxwage, rcmd.sttag1_id, rcmd.sttag2_id, rcmd.sttag3_id from employer as e inner join rcmdemployer as rcmd ' +
          'on e.recommend_id = rcmd.recommend_id where e.employer_id = ?;';

        conn.query(sql, e_id, (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          else if(results[0] == undefined){
            return innercallback({message: 'recommendation does not inserted.'});
          }

          var obj = {
            corp: [],
            work: [],
            style: []
          };

          obj.corp = results[0].corpctg_id;
          obj.work[0] = results[0].w1ctg_id;
          obj.work[1] = results[0].w2ctg_id;
          obj.mintime = results[0].mintime;
          obj.maxtime = results[0].maxtime;
          obj.minwage = results[0].minwage;
          obj.maxwage = results[0].maxwage;
          obj.style[0] = results[0].sttag1_id;
          obj.style[1] = results[0].sttag2_id;
          obj.style[2] = results[0].sttag3_id;

          innercallback(null, obj);
        });
      }
    ],
    function (err, results){
      if(err){
        console.log('error : ', err);
        conn.release();
        return callback(err);
      }
      conn.release();
      callback(null, results);
    }); //async.waterfall
  }); //pool.getConnection
} //Employer.getEmployerRecommend


//찜 목록 보기
//TODO
Employer.getLikeList = (e_id, callback) => {
  pool.getConnection(function (err, conn) {
    if (err) {
      console.log('error connecting : ', err);
      conn.release();
      return callback(err);
    }
    async.waterfall([
      function getID(innercallback){
        var sql = 'select employer_id as `eid` from employer where employer_id = ?;';
        conn.query(sql, e_id, (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          else if(results[0] == undefined){
            return innercallback({message : 'employer id does not exist.'});
          }
          innercallback(null, results[0].eid);
        });
      },

      function showLikelist(e_id, innercallback){
        var sql = 'select lik.designer_id as designer_id, rcmd.sttag1_id as sttag1_id, rcmd.sttag2_id as sttag2_id,' +
          'rcmd.sttag3_id as sttag3_id from likelist as lik inner join designer as d ' +
          'on lik.designer_id=d.designer_id inner join rcmddesigner as rcmd on d.recommend_id=rcmd.recommend_id '+
          'where lik.employer_id = ?;';

        conn.query(sql, e_id, (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }

          var obj = {
            designer_array: []
          };

          for (var i = 0; i < results.length; i++) {
            obj.designer_array[i] = results[i];
          }

          obj.list_num = results.length;
          console.log(obj);

          innercallback(null, obj);
        });
      }
    ],
    function (err, results){
      if (err) {
        console.log('error : ', err);
        conn.release();
        return callback(err);
      }
      conn.release();
      callback(null, results);
    }); //async.waterfall
  }); //pool.getConnection
} //Employer.getLikeList


//찜 추가
Employer.postLike = (e_id, d_id, callback) => {
  pool.getConnection(function (err, conn) {
    if (err) {
      console.log('error connecting : ', err);
      conn.release();
      return callback(err);
    }
    async.waterfall([
      function getEmployerID(innercallback) {
        var sql = 'select employer_id as `eid` from employer where employer_id = ?;';
        conn.query(sql, e_id, (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          else if(results[0] == undefined){
            return innercallback({message : 'employer id does not exist.'});
          }
          innercallback(null, results[0].eid);
        });
      },
      function getDesignerID(e_id, innercallback) {
        var sql = 'select designer_id as `d_id` from designer where designer_id = ?;';
        conn.query(sql, d_id, (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          else if(results[0] == undefined){
            return innercallback({message : 'designer id does not exist.'});
          }
          innercallback(null, e_id, results[0].d_id);
        });
      },
      function addLike(e_id, d_id, innercallback) {
        var dt = new Date();
        var d = dt.toFormat('YYYY-MM-DD');

        var sql = 'insert into likelist values(null,?,?,?);';
        conn.query(sql, [e_id, d_id, d], (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          innercallback(null, d_id);
        });
      },
      function getLikenum(params, innercallback) {
        var sql = 'select likenum from designer where designer_id =?;';
        conn.query(sql, params, (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          var likenum = results[0].likenum;
          likenum++;
          innercallback(null, likenum);
        });
      },
      function updateDesignerLikenum(params, innercallback) {
        var sql = 'update designer set likenum =? where designer_id =?;';
        conn.query(sql, [params, d_id], (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          innercallback(null, 'success');
        });
      },
      function getDesignerLikeList(params, innercallback) {
        var sql = 'select designer_id from likelist where employer_id =?;';
        conn.query(sql, e_id, (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }

          var obj = {
            designer_array: []
          };

          for (var i = 0; i < results.length; i++) {
            obj.designer_array[i] = results[i].designer_id;
          }
          obj.list_num = results.length;
          innercallback(null, obj);
        });
      }
    ],
      function (err, results) {
        if (err) {
          console.log('error : ', err);
          conn.release();
          return callback(err);
        }
        conn.release();
        callback(null, results);
      });
  });
}


//찜 해제
Employer.deleteLike = (e_id, d_id, callback) => {
  pool.getConnection(function (err, conn) {
    if (err) {
      console.log('error connecting : ', err);
      return callback(err);
    }
    async.waterfall([
      function getEmployerID(innercallback) {
        var sql = 'select employer_id as `eid` from employer where employer_id = ?;';
        conn.query(sql, e_id, (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          else if(results[0] == undefined){
            return innercallback({message : 'employer id does not exist.'});
          }
          innercallback(null, results[0].eid);
        });
      },
      function getDesignerID(e_id, innercallback) {
        var sql = 'select designer_id as `did` from designer where designer_id = ?;';
        conn.query(sql, d_id, (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          else if(results[0] == undefined){
            return innercallback({message : 'designer id does not exist.'});
          }
          innercallback(null, e_id, results[0].did);
        });
      },
      function deleteLike(e_id, d_id, innercallbackinnercallback) {
        var sql = 'delete from likelist where employer_id= ? and designer_id = ?;';
        conn.query(sql, [e_id, d_id], (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          innercallback(null, d_id);
        });
      },
      function getLikenum(params, innercallback) {
        var sql = 'select likenum from designer where designer_id =?;';
        conn.query(sql, params, (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          var likenum = results[0].likenum;
          likenum--;
          innercallback(null, likenum);
        });
      },
      function updateDesignerLikenum(params, innercallback) {
        var sql = 'update designer set likenum =? where designer_id =?;';
        conn.query(sql, [params, d_id], (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }
          innercallback(null, 'success');
        });
      },
      function getDesignerLikeList(params, innercallback) {
        var sql = 'select designer_id from likelist where employer_id =?;';
        conn.query(sql, e_id, (err, results) => {
          if (err) {
            console.log('error sql : ', err);
            return innercallback(err);
          }

          var obj = {
            designer_array: []
          };

          for (var i = 0; i < results.length; i++) {
            obj.designer_array[i] = results[i].designer_id;
          }
          obj.list_num = results.length;
          innercallback(null, obj);
        });
      }
    ],
      function (err, results) {
        if (err) {
          console.log('error : ', err);
          conn.release();
          return callback(err);
        }
        conn.release();
        callback(null, results);
      });
  });
}

module.exports = Employer;
