/* Designer model
    2016-11-22 written by ckh
    GET POST PUT DELETEdesigner prof, review data, level...
*/

var dateFormat = require('dateformat');
const pool = require('./dbConnection');
var async = require('async');
require('date-utils');
class Designer { }

// getDesignerList
/*
    1. *저장되어 있는 순서대로 select
    2. 랜덤 출력 view생성, 디자이너 추가,삭제를 고려해야하는 큰 문제가 있음
*/

Designer.getDesignerList = (callback) => {
    //FIXME
    pool.getConnection(function (err, conn) {
        if (err) {
            console.log('error connecting : ', err);
            conn.release();
            return callback(err);
        }
        var sql = 'select d.designer_id, rcmd.sttag1_id,rcmd.sttag2_id,rcmd.sttag3_id from designer ' +
            'as d inner join rcmddesigner as rcmd on d.recommend_id=rcmd.recommend_id limit 30;';
        conn.query(sql, function (err, results) {
            if (err) {
                conn.release();     //에러 발생시 커넥션 릴리즈
                return callback(err);
            }
            /*응답 데이터 폼 */
            var obj = {
                main: []
            };
            obj.main = results;
            conn.release();
            return callback(null, obj);
        }); //conn.query
    }); //pool.getConnection
} // getDesignerList
Designer.getDesignerInfo = (d_id, callback) => {
    pool.getConnection( (err,conn) => {
        if (err) {
            console.log('error connecting : ', err);
            conn.release();
            return callback(err);
        }
        var sql = 'select nickname, introduce, portimg_number from designer where designer_id=?'
        conn.query(sql, d_id, (err,result) =>{
            if(err){
            conn.release();
            return callback(err);
        }
        conn.release();
        var obj ={profile:{}};
        obj.profile=result[0];
        return callback(null,obj)
        });

    });
}
Designer.getDesignerInfoDetail = (d_id, callback) => {
    pool.getConnection(function (err, conn) {
        if (err) {
            console.log('error connecting : ', err);
            conn.release();
            return callback(err);
        }
        async.series(
            [
                function profdata(innercallback) {
                    var sql = 'select d.nickname as nickname,d.level as level,d.auth as auth,d.introduce as introduce, d.portimg_number as portnumber,' +
                        'rcmd.sttag1_id as sttag1,rcmd.sttag2_id as sttag2,rcmd.sttag3_id as sttag3,rcmd.w1ctg_id as work1,rcmd.w2ctg_id as work2,' +
                        'rcmd.minwage as minwage,rcmd.maxwage as maxwage,rcmd.mintime as mintime,rcmd.maxtime as maxtime' +
                        ' from designer as d inner join rcmddesigner as rcmd on d.recommend_id = rcmd.recommend_id where d.designer_id = ?;';
                    conn.query(sql, d_id, function (err, data) {
                        if (err) {
                            return innercallback(err);
                        }
                        else if(data[0] == undefined){
                            return innercallback({message : 'recommend_id is null'});
                        }
                        var obj = { result: {} };
                        obj.result = data[0];
                        //for response data
                        var cov_obj = {
                            nickname: obj.result.nickname,
                            level: obj.result.level,
                            auth: obj.result.auth,
                            introduce: obj.result.introduce,
                            portnumber: obj.result.portnumber,
                            style: [obj.result.sttag1, obj.result.sttag2, obj.result.sttag3],
                            work: [obj.result.work1, obj.result.work2],
                            minwage: obj.result.minwage,
                            maxwage: obj.result.maxwage,
                            mintime: obj.result.mintime,
                            maxtime: obj.result.maxtime
                        };
                        innercallback(null, cov_obj);
                    });
                },
                function revdata(innercallback) {
                    var sql = 'select employer_id, contents,score,reviewdate from dreview where designer_id = ? order by(reviewdate) desc limit 3;';
                    conn.query(sql, d_id, function (err, data) {
                        if (err) {
                            return innercallback(err);
                        }
                        innercallback(null, data);
                    });
                },
            ], //콜백 배열
            function (err, results) {
                if (err) {
                    console.log('에러', err.message);
                    conn.release(); //에러 발생시 커넥션 릴리즈 해줘야함.
                    return callback(err);
                }

                for (var i = 0; i < results[1].length; i++) {
                    var str = dateFormat(results[1][i].reviewdate, 'yyyy-mm-dd');
                    results[1][i].reviewdate = str;
                }

                var obj = {
                    info: {},
                    review: []
                };

                obj.info = results[0];
                obj.review = results[1];
                conn.release();
                return callback(null, obj);
            } //results callback
        ); //async
    }); //poolConeection
} //getDesignerInfo

Designer.postDesignerRecommend = (obj, d_id, callback) => {
    pool.getConnection(function (err, conn) {
        if (err) {
            console.log('employer rcmd input error:', err.message);
            conn.release();
            return callback(err);
        }
        async.waterfall(
            [
                function insertRcmd(callback) {

                    var dt = new Date();    //현재 날짜
                    var d = dt.toFormat('YYYY-MM-DD');  //날짜 출력 form 지정

                    var sql = 'insert into rcmddesigner values(null,?,?,?,null,null,?,?,?,?,?,?,?,?,?,?)';

                    conn.query(sql, [d_id, obj.corp[0], obj.corp[1], obj.work[0], obj.work[1], obj.mintime, obj.maxtime,
                        obj.minwage, obj.maxwage, obj.style[0], obj.style[1], obj.style[2], d], function (err, result) {
                            if (err) {
                                return callback(err);
                            }
                            callback(null, 'success');
                        });
                },
                function getID(params, callback) {
                    var sql = 'SELECT LAST_INSERT_ID() as lastId;';
                    //select LAST_INSERT_ID() -> 마지막으로 삽입된 primarykey를 가져옴
                    // 하나의 connection에서만 결과값을 가져오기 때문에 다른 커넥션 데이터를 가져올 걱정은 하지 않아도 된다.
                    conn.query(sql, function (err, result) {
                        if (err) {
                            return callback(err);
                        }
                        callback(null, result[0].lastId);
                    });
                },
                function updateDesignerTable(params, callback) {
                    var sql = 'update designer set recommend_id= ? where designer_id = ?;';
                    conn.query(sql, [params, d_id], function (err, result) { // 쿼리문 안에 들어가는 파라미터 2개 이상일시 배열로 넣어준다.
                        if (err) {
                            return callback(err);
                        }
                        callback(null, result);
                    });
                }
            ],
            function (err, result) {
                if (err) {
                    console.log(err.message);
                    conn.release();
                    return callback(err);
                }
                var resData = {
                    msg: 'recommendation insert success'
                };
                conn.release();
                return callback(null, resData);
            }
        );
    });
}

Designer.getDesigerRecommend = (d_id, callback) => {
    pool.getConnection(function (err, conn) {
        if (err) {
            console.log('err : ', err.message);
            conn.release();
            return callback(err);
        }
        var sql = 'select rcmd.corp1ctg_id,rcmd.corp2ctg_id,rcmd.w1ctg_id,rcmd.w2ctg_id,rcmd.mintime,rcmd.maxtime,rcmd.minwage,rcmd.maxwage,' +
            'rcmd.sttag1_id,rcmd.sttag2_id,rcmd.sttag3_id from designer as d inner join ' +
            'rcmddesigner as rcmd on d.recommend_id=rcmd.recommend_id where d.designer_id=?;';
        conn.query(sql, d_id, function (err, result) {
            if (err) {
                conn.release();
                return callback(err);
            }
            else if(result[0] == undefined){
                conn.release();
                return callback({message : 'recommend_id is null'});
            }
            var obj = {
                corp: [],
                work: [],
                minwage: result[0].minwage,
                maxwage: result[0].maxwage,
                style: []
            };
            obj.corp[0] = result[0].corp1ctg_id;
            obj.corp[1] = result[0].corp2ctg_id;
            obj.work[0] = result[0].w1ctg_id;
            obj.work[1] = result[0].w2ctg_id;
            obj.mintime = result[0].mintime;
            obj.maxtime = result[0].maxtime;
            obj.style[0] = result[0].sttag1_id;
            obj.style[1] = result[0].sttag2_id;
            obj.style[2] = result[0].sttag3_id;

            conn.release();
            return callback(null, obj);
        });
    });
}

Designer.putDesignerRecommend = (obj, d_id, callback) => {
    pool.Connection(function (err, conn) {
        if (err) {
            console.log('err', err.message);
            conn.release();
            return callback(err);
        }
        async.waterfall(
        [
            function getRecommendID(innercallback){
                var sql = 'select recommend_id from designer where designer_id=? ;';
                conn.query(sql,d_id,(err, result) => {
                    if(err){
                        console.log('query error : ',err);
                        return innercallback(err);
                    }
                    innercallback(null,result);
                });
            },
            function editRecommend(recommend_id, innercallback) {
                var dt = new Date();
                var d = dt.toFormat('YYYY-MM-DD');

                var sql = 'update rcmdemployer set corp1ctg_id =?,corp2ctg_id = ?,w1ctg_id =?,w2ctg_id =?,mintime = ?,maxtime =? ' +
                    'minwage = ?, maxwage = ?, sttag1_id = ?, sttag2_id = ?, sttag3_id = ?, date = ? where recommend_id = ?';
                conn.query(sql, [obj.corp[0], obj.corp[1], obj.work[0], obj.work[1],
                obj.mintime, obj.maxtime, obj.minwage, obj.maxwage, obj.style[0], obj.style[3], obj.style[2], d
                ], (err, results) => {
                    if (err) {
                        console.err('error sql : ', err);
                        return innercallback(err);
                    }
                    var resData = {
                        msg: 'success'
                    };
                    innercallback(null, resData);
                });
            }
        ],function(err,result){
            if(err){
                conn.release();
                return callback(err);
            }
            conn.release();
            return callback(null,result);
        });
    });
}

Designer.getLevel = (d_id, callback) => {
    pool.getConnection(function (err, conn) {
        if (err) {
            console.log('designer getLevel error: ', err.message);
            conn.release();
            return callback(err);
        }
        async.series(
            [
                function leveldata(innercallback) {
                    var sql = 'select level,level_exp from designer where designer_id = ?';
                    conn.query(sql, d_id, function (err, result) {
                        if (err) {
                            return innercallback(err);
                        }
                        return innercallback(null, result[0]);
                    });
                },
                function logdata(innercallback) {
                    var sql = 'select contents,date from levellog where designer_id = ? order by(date) desc limit 12';
                    conn.query(sql, d_id, function (err, result) {
                        if (err) {
                            return innercallback(err);
                        }
                        return innercallback(null, result);
                    });
                }
            ],
            function (err, result) {
                if (err) {
                    console.log(err.message);
                    conn.release();
                    return callback(err);
                }
                var obj = {
                    level: {},
                    exp: result[0].level_exp,
                    log: []
                }; //date형은 쿼리했을때 string으로 들어 오지 앟기 때문에 작업을 추가로 해줘야함
                obj.level = result[0].level;
                obj.exp = result[0].level_exp;
                obj.log = result[1];
                for (i = 0; i < obj.log.length; i++) {
                    var str = dateFormat(obj.log[i].date, 'yyyy-mm-dd');
                    obj.log[i].date = str;
                }
                conn.release();
                return callback(null, obj);
            }
        );
    });
} //Designer.getLevel

Designer.getReviewMore = (d_id, callback) => {
    pool.getConnection(function (err, conn) {
        if (err) {
            console.log('designer getLevel error: ', err.message);
            conn.release();
            return callback(err);
        }
        var sql = 'select employer_id,contents,score,reviewdate from dreview where designer_id =? order by(reviewdate) desc;';
        conn.query(sql, d_id, function (err, results) {
            if (err) {
                conn.release();
                return callback(err);
            }
            var obj = {
                reviewlist: []
            };
            obj.reviewlist = results;
            for (i = 0; i < obj.reviewlist.length; i++) {
                var str = dateFormat(obj.reviewlist[i].reviewdate, 'yyyy-mm-dd');
                obj.reviewlist[i].reviewdate = str;
            }
            conn.release();
            return callback(null, obj);
        }); //conn.query
    }); //pool
} //Designer.getReviewmore

Designer.postReview = (obj, d_id, callback) => {
    pool.getConnection(function (err, conn) {
        if (err) {
            console.log('postReview', err.message);
            conn.release();
            return callback(err);
        }
        var dt = new Date();
        var d = dt.toFormat('YYYY-MM-DD');
        var sql = 'insert into ereview values(null,?,?,?,?)';
        conn.query(sql, [obj.employer_id, d_id, obj.contents, obj.score, d], function (err, result) {
            if (err) {
                console.log(err.message);
                conn.release();
                return callback(err);
            }
            conn.release();
            return callback(null, result);
        }); //conn.query

    });
}
module.exports = Designer;
