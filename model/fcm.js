const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
var FCM = require('fcm-push');
const pool = require('./dbConnection');
//상헌이형과 붙일때 apikey 변경해야함
const apiKey = 'AAAATwdKOqg:APA91bHFZXoKusO8a5niP_J_Phy5HdKlQ2ER4GOEr_q_ByVT3MOjjWKSyiUvc01SC7FEgaTvZGMpZtf_bSBJPvCy9hAhsFkmRnZjYtN2hzM3JcRzYrI0N2AcSpAGHlFQMYmAEMnJoMY4VN3YvX9sVcLD3Fg3ttvR9A';

class Fcm{

}

Fcm.sendMsgPush = (userpart,target_id,callback) => {
    var sql;
    var title;
    if(!(userpart==0 || userpart==1)){
        return callback(err);
    }
    if(userpart == 0) {
        sql = 'select fcmid from user where employer_id=?;';
        title = '디자이너로부터 새로운 메세지가 왔습니다.'
    }
    else if(userpart == 1) {
        sql = 'select fcmid from user where designer_id=?;';
        title = '소상공인으로부터 새로운 메세지가 왔습니다.'
    }
    pool.getConnection((err, conn) => {
        conn.query(sql, target_id, (err, results) => {
            if (err) {
                console.log('error sql : ', err);
                conn.release();
                return callback(err);
            }
            if(results[0]===undefined) return callback({msg:'fcmid x'});
            var tokens = results[0].fcmid;
            conn.release();
            console.log('tokens : ', tokens, ' msg : ', msg);
            const message = {
                // 메세지를 받을 수 없는 상태
                // 전원 꺼진 상태, 네트워크 오프라인
                delay_while_idle: true,  // 기기 비활성화 상태시 메세지 전송 늦추기
                time_to_live: 86400,   // 메세지 수명
                to: tokens,
                notification: {
                    title:title,
                    text: msg
                },
                data: {
                    'value': 'New Message'
                },
                //collapse_key: "collapse_key" // 전송되지 않은 기존 메세지를 최신 메세지로 대체 가능
            };

            const fcm = new FCM(apiKey);

            fcm.send(message, (err, results) => {

                if (err) {
                    console.error('Error : ', err);
                    res.status(500).send({ msg: err.message });
                    return callback(err);
                }
                callback(null,results);
            });
        });
    });

}
module.exports = Fcm;
