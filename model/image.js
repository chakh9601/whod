/* Uploading image to S3

*/
var async = require('async');
var formidable = require('formidable');
const express = require('express');
var fs = require('fs');
var pathUtil = require('path');
var easyimg = require('easyimage');
var pool = require('./dbConnection');
var AWS = require('aws-sdk');
const config = require('../config/s3config.js');

AWS.config.region = config.region;
AWS.config.accessKeyId = config.accessKeyId;
AWS.config.secretAccessKey = config.secretAccessKey;

// Listup All Files
var s3 = new AWS.S3();

var bucketName = 'whod';

class Image{
}
/*
set 값의 의미
1 : designer prof upload
2 : designer portpolio upload
3 : designer authimg upload
4 : employer prof upload
5 : employer storeimg upload
*/

Image.ImageUpload = (set, req, callback) => {
  var uploadDir = __dirname+'/upload';
  var thumbnailDir = __dirname+'/thumbnail';

  if (!(fs.existsSync(uploadDir) && fs.existsSync(thumbnailDir))) {
     console.error('upload, thumbnail 폴더 없음.');
     err.message = 'undefined folder';
     return callback(err);
  }

  var userpath;
  var filename;

  var imgnum;
    //set값과,id값으로 이미지 저장되는 경로 설정

      async.waterfall([
            function settings(callback){
                switch (set) {
                    case 1:
                      userpath = 'designer/';
                      filename = 'profimg';
                      callback(null,1);
                      break;
                    case 2:
                      userpath = 'designer/';
                      /*결과 확인해 봐야함*/
                      async.series([
                          function portdata(callback){
                            pool.getConnection(function(err, conn){
                              if(err){
                                  conn.release();
                                  return callback(err);
                              }
                              var sql = 'select portimg_number from designer where designer_id= ?';
                              conn.query(sql,req.params.id,function(err,result){
                                if(err){
                                    conn.release();
                                  return callback(err);
                                }
                                conn.release();
                                callback(null,result[0].portimg_number);
                              });
                            });
                          }
                        ],
                        function(err,result){
                          if(err){
                            console.log(err.message);
                            return;
                          }
                          imgnum=result;
                          console.log(imgnum[0]);
                          filename = 'portpolio/'+(imgnum[0]+1);
                          callback(null,1);
                        }
                      );
                      break;
                    case 3:
                      userpath = 'designer/';
                      filename = 'authimg';
                      callback(null,1);
                      break;
                    case 4:
                      userpath = 'employer/';
                      filename = 'profimg';
                      callback(null,1);
                      break;
                    case 5:
                      userpath = 'employer/';
                      async.series([
                          function portdata(callback){
                            pool.getConnection(function(err, conn){
                              if(err){
                                  conn.release();
                                return callback(err);
                              }

                              var sql = 'select storeimg_number from employer where employer_id= ?;';

                              conn.query(sql,req.params.id,function(err,result){
                                if(err){
                                    conn.release();
                                  return callback(err);
                                }
                                conn.release();
                                callback(null,result[0].storeimg_number);
                              });
                            });
                          }
                        ],
                        function(err,result){
                          if(err){
                            console.log(err.message);
                            return;
                          }
                          imgnum=result[0];
                          filename = 'storeimg/'+(imgnum+1);
                          callback(null,1);
                        }
                      );
                      break;
                  //default:
                }
                  //callback(null,1);
              },
             // 바디 파싱
             function (value,callback) {
                var form = new formidable.IncomingForm();
                form.encoding = 'utf-8';
                form.uploadDir = uploadDir;
                form.multiples = true;
                form.keepExtensions = true;
                form.parse(req, function (err, fields, files) {
                   if ( err ) {
                      return callback(err, null);
                   }

                   // 임시 폴더로 업로드된 파일
                   var file = files.file;
                   callback(null, file);
                });
             },
             // 썸네일 생성
             function(file, callback) {
                var fileName = file.name;
                var filePath = file.path;

                var thumbnailFilePath = thumbnailDir + pathUtil.sep + fileName;

                // 임시 파일에서 썸네일 생성
                easyimg.thumbnail({
                   src:filePath,
                   dst:thumbnailFilePath,
                   width:100
                }).then(function(image){
                   callback(null, file, thumbnailFilePath);
                }, function(err) {
                   callback(err);
                });
            },
             // S3에 이미지 업로드
        function(file, thumbnailFilePath, callback) {
           var contentType = file.type;
           var readStream_image = fs.createReadStream(file.path);

           // 버킷 내 객체 키 생성
           var itemKey_image = userpath + req.params.id +'/' + filename;

           //원본 이미지
           var params_image = {
              Bucket: bucketName, // 필수
              Key: itemKey_image,     // 필수
              ACL: 'public-read',
              Body: readStream_image,
              ContentType:contentType
           }

           s3.putObject(params_image, function(err, data) {
              if ( err ) {
                 console.error('S3 PutObject Error', err);
                 callback(err, null);
              }

              fs.unlink(file.path,function(err){
                    if(err){
                          return console.log(err);
                    }
                    callback(null, thumbnailFilePath,contentType);
              });

           });
        },
        function(thumbnailFilePath, contentType, callback){

            var readStream_thumbnail = fs.createReadStream(thumbnailFilePath);

            var itemKey_thumbnail = userpath+ req.params.id+'/' + filename+'_thum';

            //썸네일 이미지
            var params_thumbnail = {
                Bucket: bucketName, // 필수
                Key: itemKey_thumbnail,     // 필수
                ACL: 'public-read',
                Body: readStream_thumbnail,
                ContentType:contentType
           }

           s3.putObject(params_thumbnail, function(err, data) {
              if ( err ) {
                 console.error('S3 PutObject Error', err);
                 callback(err, null);
              }
              else {
                  var imageUrl = s3.endpoint.href + bucketName  + '/' + itemKey_thumbnail;
                  fs.unlink(thumbnailFilePath,function(err){
                      if(err){
                             return console.log(err);
                       }
                 });
                 callback(null, imageUrl);
              }
           });
        },
        function(url, callback) {
            var img_path = {
                img_path : url
            }
              switch (set) {
                case 2:
                pool.getConnection(function(err,conn){
                  if(err){
                      conn.release();
                    return callback(err);
                  }
                  var sql = 'update designer set portimg_number=portimg_number+1 where designer_id=?;';
                  conn.query(sql,req.params.id,function(err,result){
                    if(err){
                        conn.release();
                      return callback(err);
                    }
                    conn.release();
                  });
                });
                break;
                case 5:
                pool.getConnection(function(err,conn){
                  if(err){
                      conn.release();
                      return callback(err);
                  }
                  var sql = 'update employer set storeimg_number=storeimg_number+1 where employer_id=?;';
                  conn.query(sql,req.params.id,function(err,result){
                    if(err){
                        conn.release();
                      return callback(err);
                  }
                    conn.release();
                  });
                });
                break;
                default:
              }
            callback(null,img_path);
        }
     ],
     //waterfall 결과 코드
     function (err,result) {
        if (err) {
           return callback(err);
        }
        var resData = {
          msg: 'success'
      }
        return callback(null, resData);
     });
}

module.exports = Image;
