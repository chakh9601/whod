# README #

### WhoD Server Repository ###

* 후디 서버 저장소
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### commit 메세지 작성 규칙###

* 주요 기능 개발 'dev issue #1 - <태그명>작업요약' 빈행을 만든뒤(엔터) 상세 작업 내용 작성
* 태그 : 추가(ADD),오류+버그 개선(FIXED), 재작업(REWORK), 코드개선+주석작업(POLISH)
* ex) 'dev issue #142 - <ADD> write scripts about get req'
* 급한 버그 or 수정 'Hot fix #1 - error fix'
* 굵직한 이슈가 아니라 코드 가독성을 높이거나, 기능에 큰 영향이 없는 이슈 'issue - 주석작업 '

### 주의사항 ####

* npm 모듈 설치시 뒤에 --save 옵션 붙여주세요. ex) npm install express --save
* 서버 킬 필요 없이 로컬에서 충분히 테스트 해보고 push만 하면 자동으로 적용되고, 재가동 합니다.
* 에러로 인해 서버 다운시 에러 해결 후nodemon mainserver.js > log/년도/월/일.log & 입력해서 재가동 해주세요.
