const express = require('express');
const Image = require('../model/image.js');

var router = express.Router();
/*refactoring plan
- req를 파라미터로 다 던지지 말고 router에서 분석하고 분석한 결과를 넘길것
- 코드의 가독성을 높이도록
*/
router.route('/designer/:id/profileimg')
.post(sendDesignerProfile);

router.route('/designer/:id/portpolio')
.post(sendDesignePortPolio);

router.route('/designer/:id/authimg')
.post(sendDesignerAuth);

router.route('/employer/:id/profileimg')
.post(sendEmployerProfile);

router.route('/employer/:id/storeimg')
.post(sendEmployerStoreimg);

module.exports = router;

function sendDesignerProfile(req,res,next){
  Image.ImageUpload(1,req,(err,result)=>{
    if(err){
      console.log('postImageUpload set code 1 error : ' ,err.message);
      return next(err);
    }
    res.send(result);
  });
}
function sendDesignePortPolio(req,res,next){
  Image.ImageUpload(2,req,(err,result)=>{
    if(err){
      console.log('postImageUpload set code 2 error : ' ,err.message);
      return next(err);
    }
    res.send(result);
  });
}
function sendDesignerAuth(req,res,next){
  Image.ImageUpload(3,req,(err,result)=>{
    if(err){
      console.log('postImageUpload set code 3 error : ' ,err.message);
      return next(err);
    }
    res.send(result);
  });
}
function sendEmployerProfile(req,res,next){
  Image.ImageUpload(4,req,(err,result)=>{
    if(err){
      console.log('postImageUpload set code 4 error : ' ,err.message);
      return next(err);
    }
    res.send(result);
  });
}
function sendEmployerStoreimg(req,res,next){
  Image.ImageUpload(5,req,(err,result)=>{
    if(err){
      console.log('postImageUpload set code 5 error : ' ,err.message);
      return next(err);
    }
    res.send(result);
  });
}
