const express = require('express');
const Employer = require('../model/employer.js');

var router = express.Router();

router.route('/employer/:id/info')
.get(showEmployerInfo);
//.put(UpdateEmployerInfo);

router.route('/employer/:id/infodetail')
.get(showEmployerInfoDetail);

router.route('/employer/:id/review')
.get(showReviewMore)
.post(addReview);

router.route('/employer/:id/recommendation')
.post(addEmployerRecommend)
.get(showEmployerRecommend);

router.route('/employer/:id/matching')
.post(requestConsulting)
.get(showMatchingHistory);

// router.route('/employer/:id/consultiing')
// .get(showConsultingHistory);

router.route('/employer/:id/like')
.get(showLikeList)
.post(addLike)
.delete(deleteLike);

module.exports = router;

function showEmployerInfo(req,res,next){
    var employer_id = req.params.id
    if (employer_id == null || employer_id == undefined) {
        res.status(400).send({message : 'invalid employer id value'});
    }
    Employer.getEmployerInfo(employer_id,(err,result)=> {
        if(err){
            console.log('employer info err',err.message);
            return next(err);
        }
        res.send(result);
    });
}
//소상공인 프로필 보기
function showEmployerInfoDetail(req, res, next) {

    var employer_id = req.params.id;

    if (employer_id == null || employer_id == undefined) {
        res.status(400).send({message : 'invalid employer id value'});
    }
    else {
        Employer.getEmployerInfoDetail(employer_id, (err, result) => {
            if (err) {
                console.log('employer infodetail err : ', err.message);
                return next(err);
            }
            res.send(result);
        });
    }
}

//소상공인 리뷰 보기
function showReviewMore(req, res, next) {

    var employer_id = req.params.id;

    if (employer_id == null || employer_id == undefined) {
        res.status(400).send({message : 'invalid employer id value'});
    }
    else {
        Employer.getReviewMore(employer_id, (err, result) => {
            if (err) {
                console.log('review more err : ', err.message);
                return next(err);
            }
            res.send(result);
        });
    }
}

//소상공인 리뷰 작성
function addReview(req, res, next) {
    var employer_id = req.params.id;

    if (employer_id == null || employer_id == undefined) {
        res.status(400).send({message : 'invalid employer id value'});
    }
    else {
        var review = req.body;

        Employer.postReview(employer_id, review, (err, result) => {
            if (err) {
                console.log('review insert err : ', err.message);
                return next(err);
            }
            res.send(result);
        });
    }
}

//추천 조건 입력과 매칭
function addEmployerRecommend(req, res, next) {
    var employer_id = req.params.id;

    if (employer_id == null || employer_id == undefined) {
        res.status(400).send({message : 'invalid employer id value'});
    }
    else {
        var rcmd = req.body;

        Employer.postEmployerRecommend(employer_id, rcmd, (err, result) => {
            if (err) {
                console.log('employer recommend insert error : ', err.message);
                return next(err);
            }
            res.send(result);
        });
    }
}

//추천 조건 보기
function showEmployerRecommend(req, res, next) {
    var employer_id = req.params.id;

    if (employer_id == null || employer_id == undefined) {
        res.status(400).send({message : 'invalid employer id value'});
    }
    else {
        Employer.getEmployerRecommend(employer_id, (err, result) => {
            if (err) {
                console.log('employer recommend show err : ', err.message);
                return next(err);
            }
            res.send(result);
        });
    }
}

//디자이너에게 작업 상담 요청
function requestConsulting(req, res, next) {
    var employer_id = req.params.id;

    if (employer_id == null || employer_id == undefined) {
        res.status(400).send({message : 'invalid employer id value'});
    }
    else {
        var obj = req.body;

        Employer.postDesignerMatching(employer_id, obj, (err, result) => {
            if (err) {
                console.log('employer consulting request err :', err.message);
                return next(err);
            }
            res.send(result);
        });
    }
}

//매칭 히스토리 보기
function showMatchingHistory(req, res, next) {
    var employer_id = req.params.id;

    if (employer_id == null || employer_id == undefined) {
        res.status(400).send({message : 'invalid employer id value'});
    }
    else {
        Employer.getMatchingHistory(employer_id, (err, result) => {
            if (err) {
                console.log('matching history show err : ', err.message);
                return next(err);
            }
            res.send(result);
        });
    }
}
// function showConsultingHistory(req,res,next){
//     if (employer_id == null || employer_id == undefined) {
//          res.status(400).send({message : 'invalid employer id value'});
//     }
//     Employer.getConsultingHistory(req.params.id, (err,result) => {
//         if(err){
//             console.log(err);
//             return next(err);
//         }
//         res.send(result);
//     });
//
//  }
//찜 목록 보기
function showLikeList(req, res, next) {
    var employer_id = req.params.id;

    if (employer_id == null || employer_id == undefined) {
        res.status(400).send({message : 'invalid employer id value'});
    }
    else {
        Employer.getLikeList(employer_id, (err, result) => {
            if (err) {
                console.log('likelist err : ', err.message);
                return next(err);
            }
            res.send(result);
        });
    }
}

//찜 추가
function addLike(req, res, next) {
    var employer_id = req.params.id;

    if (employer_id == null || employer_id == undefined) {
        res.status(400).send({message : 'invalid employer id value'});
    }
    else {
        var designer_id = req.body.designer_id;

        Employer.postLike(employer_id, designer_id, (err, result) => {
            if (err) {
                console.log('like input error : ', err.message);
                return next(err);
            }
            res.send(result);
        });
    }
}

//찜 해제
function deleteLike(req, res, next) {

    var employer_id = req.params.id;

    if (employer_id == null || employer_id == undefined) {
        res.status(400).send({message : 'invalid employer id value'});
    }
    else {

        var designer_id = req.body.designer_id;

        Employer.deleteLike(employer_id, designer_id, (err, result) => {
            if (err) {
                console.log('like delete error : ', err.message);
                return next(err);
            }
            res.send(result);
        });
    }
}
