const express = require('express');
const Designer = require('../model/designer.js');

var router = express.Router();

router.route('/designer')
.get(showDesignerList);

router.route('/designer/:id/info')
.get(showDesignerInfo);
//.put(UpdateDesigerInfo);
router.route('/designer/:id/infodetail')
.get(showDesignerInfoDetail);

router.route('/designer/:id/review')
.get(showReviewMore)
.post(sendReview);

router.route('/designer/:id/recommendation')
.get(showDesignerRecommend)
.post(sendDesignerRecommend)
.put(modifyDesignerRecommend);

router.route('/designer/:id/level')
.get(showLevel);

module.exports = router;

/* 해야할것
1. 후기작성 페이지
2. 추천 정보 수정
*/
function showDesignerList(req,res,next){
  Designer.getDesignerList((err,results) => {
    if(err){
      console.log('designerList error : ' , err.message);
      return next(err);
    }
    res.send(results);
  });
}
function showDesignerInfo(req,res,next){
    if( req.params.id=='null' ||
        req.params.id == null ||
        req.params.id == undefined ) res.status(400).send({message : 'invalid designer id req'});
    else{
        Designer.getDesignerInfo(req.params.id, (err,result) => {
            if(err){
                console.log('designerInfo error :',err.message);
                return next(err);
            }
            res.send(result);
        });
    }
}

function showDesignerInfoDetail(req,res,next){
    if( req.params.id=='null' ||
        req.params.id == null ||
        req.params.id == undefined ) res.status(400).send({message : 'invalid designer id req'});
    else{
        Designer.getDesignerInfoDetail(req.params.id, (err,result) => {
            if(err){
                console.log('designerInfo error :',err.message);
                return next(err);
            }
            res.send(result);
        });
    }
}

function sendDesignerRecommend(req,res,next){
    if( req.params.id=='null' ||
        req.params.id == null ||
        req.params.id == undefined ) res.status(400).send({message : 'invalid designer id req'});
    else {
      var obj = req.body;
      var id = req.params.id;
      Designer.postDesignerRecommend(obj,id,(err,result) => {
        if(err){
          console.log('designer rcmd input error : ',err.message);
          return next(err);
        }
        res.send(result);
      });
  }
}

function showDesignerRecommend (req,res,next){
    if( req.params.id=='null' ||
        req.params.id == null ||
        req.params.id == undefined ) res.status(400).send({message : 'invalid designer id req'});
    else {
      Designer.getDesigerRecommend (req.params.id,(err,result)=>{
        if(err){
          console.log('designer get rcmd error : ',err.message);
          return next(err);
        }
        res.send(result);
      });
  }
}

function modifyDesignerRecommend(req,res,next){
    if( req.params.id=='null' ||
        req.params.id == null ||
        req.params.id == undefined ) res.status(400).send({message : 'invalid designer id req'});
    else {
        var obj = req.body;
      var id = req.params.id;
      Designer.putDesignerRecommend(obj,id,(err,result)=>{
        if(err){
          console.log('designer modify rcmd error:',err.message);
          return next(err);
        }
        res.send(result);
      });
  }
}

function showLevel(req,res,next){
    if( req.params.id=='null' ||
        req.params.id == null ||
        req.params.id == undefined ) res.status(400).send({message : 'invalid designer id req'});
    else{
      Designer.getLevel(req.params.id, (err,result) => {
        if( err ){
          console.log('getLevel error : ',err.message);
          return next(err);
        }
        res.send(result);
  });
  }
}

function showReviewMore(req,res,next){
    if( req.params.id=='null' ||
        req.params.id == null ||
        req.params.id == undefined ) res.status(400).send({message : 'invalid designer id req'});
    else {
      Designer.getReviewMore(req.params.id,(err,results) => {
        if( err ){
            console.log('getReviewMore error : ',err.message);
            return next(err);
        }
        res.send(results);
      });
  }
}

function sendReview(req,res,next){
    if( req.params.id=='null' ||
        req.params.id == null ||
        req.params.id == undefined ) res.status(400).send({message : 'invalid designer id req'});
    else {
      Designer.postReview(req.body,req.params.id,(err,result)=>{
        if(err){
          console.log('post Review error : ',err.message);
          return next(err);
        }
        res.send(result);
      });//Designer.postReview
  }
}
