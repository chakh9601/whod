const express = require('express');
const User = require('../model/user.js');

var router = express.Router();
router.route('/register/designer')
.post(designerSignUp);

router.route('/register/employer')
.post(employerSignUp);

router.route('/register/emailcheck/:email')
.get(emailDuplicate);

/*router.route('/register/univcheck/')
.get(univallFinding);
*/
/*router.route('/register/:univ/univcheck')
.get(univFinding);
*/
router.route('/login')
.post(Login);

router.route('/designer/:id')
.post(designerJoin);

router.route('/employer/:id')
.post(employerJoin);

function emailDuplicate(req,res,next){
    var data =req.params.email;
    User.emailcheck(data,(err,result)=>{
        if(err){
            console.log('email checking error :',err);
            return next(err);
        }
        res.send(result);
    });
}

function Login(req,res,next){

    User.SignIn(req.body.email,req.body.password,(err,result) => {
        if(err){
            console.log('login error : ',err);
            return next(err);
        }
        res.send(result);
    });
}

function designerSignUp(req,res,next){
    var obj = req.body;
    User.postNewDesigner(obj,(err,result) => {
        if(err){
            console.log('account sign up error : ',err);
            return next(err);
        }
        res.send(result);
    });
}

function employerSignUp(req,res,next){
    var obj = req.body;
    User.postNewEmployer(obj,(err,result) => {
        if(err){
            console.log('account sign up error : ',err);
            return next(err);
        }
        res.send(result);
    });
}

function univFinding(req,res,next){
    var obj = req.params.univ;
    console.log(obj);
    res.send(obj);
}

function designerJoin(req, res, next){
    var id = req.params.id;

    User.postJoin(1, id, (err, result) =>{
        if(err){
            console.log('postJoin set code 1 error: ', err);
            return next(err);
        }
        res.send(result);
    });
}

function employerJoin(req, res, next){
    User.postJoin(2, req.params.id, (err, result) =>{
        if(err){
            console.log('postJoin set code 2 error: ', err);
            return next(err);
        }
        res.send(result);
    });
}



module.exports = router;
